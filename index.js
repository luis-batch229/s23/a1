let trainer = {};

trainer.name = 'ash ketchum';
trainer.age = 10;
trainer.pokemon = ['charmander', 'squirtle', 'cobray', 'crabby',]
trainer.friends = {
  kanto: ['brock', 'misty'],
  hoenn: ['may', 'max']
}


trainer.talk = function () {
  console.log('Pikachu! I choose you!');
}

console.log(trainer);

console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);


console.log('Result of talk method');
trainer.talk();


function Pokemon(name, level) {

  this.name = name;
  this.level = level;
  this.health = 3 * level;
  this.attack = 1.5 * level;

  this.tackle = function (enemy) {
    console.log(this.name + " tackled " + enemy.name + ".");
  }

  this.faint = function () {
    console.log(this.name + " fainted");
  }

}


let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let mewtwo = new Pokemon("Mewtwo", 8);
console.log(mewtwo);


let geodude = new Pokemon("Geodude", 100);
console.log(geodude);

geodude.tackle(pikachu);

geodude.tackle(mewtwo);
mewtwo.faint();


